echo "Beginning Parsing Tests:"
echo "Scripts with Correct Syntax:"
rm -rf logs
mkdir -p logs
for filename in *.tss; do	
	echo "Running $1 with input $filename"
	./../$1 "$filename" &> logs/$filename.out	
	rv=$?
	if [ $rv -eq "1" ]
	then
		echo "Failed test with input $filename"
	fi
done
echo "Scripts with Incorrect Syntax:"
for filename in *.tsf; do
	echo "Running $1 with input $filename"
	./../$1 "$filename" &> logs/$filename.out	
	rv=$?
	if [ $rv -eq "0" ]
	then
		echo "Failed test with input $filename"
	fi
done
echo "Parsing Tests Complete."
